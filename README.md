# rpsur

<!-- badges: start -->

<!-- badges: end -->

The goal of rpsur is to fit random parameters SUR models with partially observed observations. We consider three different models: RPSUR, RP Endogenous SUR and RP Quadratic SUR.

## Installation

You can install the development version of rpsur like so:

``` r
# install.package(devtools)
# devtools::install_gitlab("umr-smart/r_packages/rpsur", host = "https://forgemia.inra.fr")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(rpsur)
library(dplyr)
data(my_rpsur_data)
data <- my_rpsur_data
### computation of time constant value of sau
time_constant_data <- aggregate(cbind(sau,k) ~ id, data = data, FUN = mean) %>%
  mutate(sau_i_center = sau - mean(sau), k_i_center = k - mean(k)) %>%
  rename(sau_i = sau, k_i = k) %>%
  select(id, sau_i, k_i, sau_i_center, k_i_center)
data <- merge(data, time_constant_data, by = "id")


idtime <- c("id","year")
weight <- NULL
crop_yield <- c("y1","y2","y3")
crop_input <- list(x1=c("x11","x12","x13"), x2=c("x21","x22","x23"))
crop_acreage <- c("s1","s2","s3")
crop_price  <- c("p1_lag","p2_lag","p3_lag")
crop_input_price  <- list(w1=c("w11","w12","w13"), w2=c("w21","w22","w23"))
crop_subsidy  <- c("sub1","sub2","sub3")
crop_yield_input_indvar <- list(zyx1=c("k", "temp_mean", "precip"),
                                zyx2=c("k", "temp_mean", "precip"),
                                zyx3=c("k", "temp_mean", "precip"))

crop_rp_indvar <-  c("sau_i_center")
distrib_method_beta_yx  <-  "lognormal"
distrib_method_alpha_x  <- "spherical"
sim_method <-  "map_imh"
calib_method <-  "cmode"
saem_control  <- list(estim_rdraw = 500,
                     stde_rdraw = 1000,
                     nb_SA = 100,
                     nb_smooth = 100,
                     nb_burn_saem = 10,
                     nb_burn_sim = 30)
sur_method <- "ensur" #"sur", "ensur", "quad"
par_init <-  list()
crop_name <- c("crop1", "crop2", "crop3")

## estimation of model 
start_fit <- Sys.time()
oplan <- future::plan(future::multisession, workers = 10 )
future::plan(future::multisession, workers = 8)
fit <- surp_model_yx(data = data,
                    idtime,
                    weight = NULL,
                    crop_yield = crop_yield,
                    crop_input = crop_input,
                    crop_acreage = crop_acreage,
                    crop_price = crop_price,
                    crop_input_price = crop_input_price,
                    crop_subsidy = crop_subsidy,
                    crop_yield_input_indvar = crop_yield_input_indvar,
                    crop_rp_indvar = crop_rp_indvar,
                    distrib_method_beta_yx = distrib_method_beta_yx, #c("lognormal","normal","censored-normal"),
                    distrib_method_alpha_x = distrib_method_alpha_x, #c("spherical","logchol"),
                    sim_method = sim_method, #c("mhrw_imh","mhrw", "marg_imh", "map_imh","nuts"),
                    calib_method = calib_method, #c("cmode","cmean"),
                    saem_control = saem_control,
                    sur_method = sur_method,
                    par_init = list(),
                    crop_name = crop_name)
                    on.exit(plan(oplan))
time_fit <- Sys.time() - start_fit
time_fit
                        
```

### Computation of simulated R2

``` r
data        <- fit$data
crop_name   <- fit$namesInmodel$crop_name
idtime      <- fit$namesInmodel$idtime
nb_crop     <- fit$opt$nb_crop  # number of crops
nb_input    <- fit$opt$nb_input # number of inputs"
crop_yield  <- fit$namesInmodel$crop_yield # name of crop yield
crop_input  <- fit$namesInmodel$crop_input # name of input
yxapprox    <- fit$est_calib$yx_hat # prediction of yield with yield level for non produced crops
yxapprox_z  <- fit$est_calib$yx_zero_hat  # prediction of input with iput use level for non produced crops
yxobs_z     <- data[,c(idtime, crop_yield, unlist(crop_input))]
yxobs_approx_z <- merge(yxobs_z, yxapprox_z, by = idtime)
yapprox_z      <- yxobs_approx_z[,names(yxapprox_z)]
yxobs_z        <- yxobs_approx_z[,names(yxobs_z)]

### R2 in the sample
R2_yx_sample <- sapply(1:(nb_crop*(nb_input + 1)), function(p){
 res <- summary(lm(yxobs_z[,p + 2] ~ yapprox_z[,p+2]))
 res$r.squared
})
R2_yx_sample <- matrix(R2_yx_sample, byrow = FALSE, nrow = nb_crop)
row.names(R2_yx_sample) <- crop_name
colnames(R2_yx_sample) <- c("yield", paste0("input",sep="_",1:nb_input))
R2_yx_sample

### R2 when crop is produced
R2_yx_prod <- sapply(1:(nb_crop*(nb_input + 1)), function(p){
 var_ind <- as.matrix(NA^(yxobs_z[,p+2]==0)*yapprox_z[,p+2]) # transform 0 in NA
 res <- summary(lm(yxobs_z[,p + 2] ~ var_ind))
 res$r.squared
})

R2_yx_prod <- matrix(R2_yx_prod, byrow = FALSE, nrow = nb_crop)
row.names(R2_yx_prod) <- crop_name
colnames(R2_yx_prod) <- c("yield", paste0("input",sep="_",1:nb_input))
R2_yx_prod
```

### Computation of estimated parameters' standard error

``` r
## mean parameters of the drandom parameters distribution
beta_b_stde <- sqrt(diag(solve(fit$est_pop$I_ruud_beta_b)))
beta_b <- cbind(fit$est_pop$par$beta_b,beta_b_stde)
colnames(beta_b) <- c("Estimate", "Stderr")

if(!is.null(fit$est_pop$p$beta_d)){
 beta_d_stde <- sqrt(diag(solve(fit$est_pop$I_ruud_beta_d)))
 beta_d <- cbind(fit$est_pop$par$beta_d,beta_d_stde)
 colnames(beta_d) <- c("Estimate", "Stderr")
}

## Random parameters variances
omega_b_stde <- sqrt(diag(solve(fit$est_pop$I_ruud_omega_b)))
omega_b_stde <- diag(ks::invvech(omega_b_stde))
diag_omega_b <- cbind(diag(fit$est_pop$par$omega_b),omega_b_stde)
row.names(diag_omega_b) <- row.names(fit$est_pop$par$omega_b)
colnames(diag_omega_b) <- c("Estimate", "Stderr")

## deltayx
delta_yx_stde <- sqrt(diag(solve(fit$est_pop$I_ruud_delta_yx)))
delta_yx_stde <- matrix(delta_yx_stde, byrow = FALSE, ncol = nb_input + 1)
delta_y <- cbind(fit$est_pop$par$delta_y,delta_yx_stde[,1])
colnames(delta_y) <- c("Estimate", "Stderr")
delta_x <- lapply(1:nb_input, function(k) {
 dd <- cbind(fit$est_pop$par$delta_x[[k]],delta_yx_stde[, k + 1])
 colnames(dd) <- c("Estimate", "Stderr")
 dd
})
#delta_yx <- cbind(delta_y, Reduce("cbind", delta_x))

##omega_u_yx
## Random parameters variances
omega_u_yx_stde <- sqrt(diag(solve(fit$est_pop$I_ruud_omega_u_yx)))
omega_u_yx_stde <- diag(ks::invvech(omega_u_yx_stde))
diag_omega_u_yx <- cbind(diag(fit$est_pop$par$omega_u_yx),omega_u_yx_stde)
row.names(diag_omega_u_yx) <- row.names(fit$est_pop$par$omega_u_yx)
colnames(diag_omega_u_yx) <- c("Estimate", "Stderr")
###
beta_b
if(!is.null(fit$est_pop$par$beta_d))
  beta_d
diag_omega_b
delta_y
delta_x

```

### Convergences' check

``` r
## convergence check
K_RS <- fit$opt$nb_temp + fit$opt$nb_burn_saem
K_SA <- fit$opt$nb_SA + fit$opt$nb_burn_saem
K_ITER <- fit$est_pop$nb_iter - fit$opt$nb_burn_saem
plot(tail(fit$est_pop$conv_indicators$cloglike_mean,K_ITER), type = "l")
graphics::abline(v=c(K_RS,K_SA), col=c("red" , "blue"), lty=c(1,2), lwd=c(1, 3))

# Plot of convergence indicator of  estimated mean
for(k in 1:ncol(fit$est_pop$conv_indicators$beta_b_all)){
 x <- fit$est_pop$conv_indicators$beta_b_all
 plot(x[,k], type="l", xlab = "iter", ylab = row.names(fit$est_pop$par$beta_b)[k] )
 graphics::abline(v=c(K_RS,K_SA), col=c("red" , "blue"), lty=c(1,2), lwd=c(1, 3))
}
# Plot of convergence indicator of  estimated variance of beta_i
for(k in 1:ncol(fit$est_pop$conv_indicators$beta_b_all)){
 x <- fit$est_pop$conv_indicators$omega_b_all
 plot(x[,k], type="l", xlab = "iter", ylab = row.names(fit$est_pop$par$beta_b)[k] )
 graphics::abline(v=c(K_RS,K_SA), col=c("red" , "blue"), lty=c(1,2), lwd=c(1, 3))
}
# Plot of convergence indicator of  estimated variance of epsilon_kit
for(k in 1:ncol(fit$est_pop$conv_indicators$delta_y_all)){
 x <- fit$est_pop$conv_indicators$delta__y_all
 K_RS <- fit$opt$nb_temp
 K_SA <- fit$opt$nb_SA
 plot(x[,k], type="l", xlab = "iter", ylab = row.names(fit$est_pop$par$delta_y)[k])
 graphics::abline(v=c(K_RS,K_SA), col=c("red" , "blue"), lty=c(1,2), lwd=c(1, 3))
}

# Plot of convergence indicator of  estimated variance of epsilon_kit
for(k in 1:ncol(fit$est_pop$conv_indicators$delta_x_all)){
 x <- fit$est_pop$conv_indicators$delta_x_all
 K_RS <- fit$opt$nb_temp
 K_SA <- fit$opt$nb_SA
 plot(x[,k], type="l", xlab = "iter", ylab = row.names(Reduce("rbind", fit$est_pop$par$delta_x))[k])
 graphics::abline(v=c(K_RS,K_SA), col=c("red" , "blue"), lty=c(1,2), lwd=c(1, 3))
}
```

### Calibrated value
``` r
beta_data_i <- merge(fit$est_calib$beta_b_yx_hat,fit$est_calib$beta_pxi_hat, by=idtime[1])
beta_data_it <- merge(data, beta_data_i, by = idtime[1] )

                    
```
