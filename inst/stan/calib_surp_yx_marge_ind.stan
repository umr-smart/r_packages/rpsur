#include /inst/stan/include/myfunctions.stan
data {

  int<lower=0> nb_crop;
  int<lower=0> nb_input;
  int<lower=0> nb_T;
  int<lower=0> nb_rp;
  vector[nb_rp] beta_i;
  matrix[nb_T, nb_crop] s;
  matrix[nb_T, nb_input * nb_crop] x;
  matrix[nb_T, nb_crop] y;
  matrix[nb_T, nb_crop] sp;
  matrix[nb_T, nb_crop] sub;
  matrix[nb_T, nb_crop * nb_input] iw_p;
  matrix[nb_T, nb_crop * (nb_input + 1)] piw;
  vector[nb_T] wdata;
  matrix[nb_T, nb_crop * (nb_input + 1)] zdelta_yx;
  real lo_spher_x;
  real up_spher_x;
  int distrib_method_beta_yx;
  int distrib_method_alpha_x;
  int sur_method;
}

model {

}

generated quantities{

   matrix[nb_T, nb_crop * (nb_input + 1)] yx_hat;
   matrix[nb_T, nb_crop * (nb_input + 1)] yx_zero_hat;
   matrix[nb_T, nb_crop] marge_yx_hat = rep_matrix(0, nb_T, nb_crop);
   matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
   matrix[nb_crop, nb_input * (nb_input + 1) / 2] beta_axi_v = rep_matrix(0, nb_crop, nb_input * (nb_input + 1) / 2);
   matrix[nb_crop, nb_input] beta_pxi_v = rep_matrix(0, nb_crop, nb_input);

  if(sur_method == 1){

    int idx_yx = nb_crop * (nb_input + 1);
    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];

    // Call the corresponding functions
    beta_yxi_v  = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);

    for (t in 1:nb_T) {
      /***********************************yield and input *********************/
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx;
      int start_l = 1;
      for(l in 1:(nb_input + 1)){
        regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
        start_l = start_l + nb_crop;
      }
      // Call mat_select_reg_obs function
      int N_p_obs = sum(regimeyx);
      int N_p_mis = nb_crop * (nb_input + 1) - N_p_obs;
      matrix[N_p_obs, nb_crop * (nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] yxapit = beta_yxi_v + zdelta_yxit;

      yx_hat[t] = to_vector(yxapit)';
      yx_zero_hat[t] = (select_yx_obs' * select_yx_obs * to_vector(yxapit))';

    }

  }else if(sur_method == 2){

    vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);
    int idx_yx = nb_crop * (nb_input + 1);
    int idx_px = idx_yx + (nb_crop * nb_input);

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
    vector[nb_crop * nb_input] beta_pxi = beta_i[(idx_yx + 1):idx_px];

    // Call the corresponding functions
    beta_yxi_v  = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_pxi_v  = to_matrix(beta_pxi, nb_input, nb_crop)';

    for (t in 1:nb_T) {
      /***********************************yield and input *********************/
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx;
      int start_l = 1;
      for(l in 1:(nb_input + 1)){
        regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
        start_l = start_l + nb_crop;
      }
      // Call mat_select_reg_obs function
      int N_p_obs = sum(regimeyx);
      int N_p_mis = nb_crop * (nb_input + 1) - N_p_obs;
      matrix[N_p_obs, nb_crop * (nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

      //vector[nb_crop] xbeta_px = (to_matrix(x[t], nb_crop, nb_input) .* beta_pxi_v) * rep_vector(1.0, nb_input);
      vector[nb_crop] xbeta_px = rows_dot_product(to_matrix(x[t], nb_crop, nb_input), beta_pxi_v);

      matrix[nb_crop, nb_input + 1] xbeta_px_0 = append_col(xbeta_px, rep_matrix(0, nb_crop, nb_input));

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));


      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit + xbeta_px_0;
      //beta_yxit[,1] = beta_yxit[,1] + xbeta_px;

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop , (nb_input + 1)] yxapit = beta_yxit;
      yx_hat[t] = to_vector(yxapit)';
      yx_zero_hat[t] = (select_yx_obs' * select_yx_obs * to_vector(yxapit))';

    }

  }else if(sur_method == 3){

    vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);
    int idx_yx = nb_crop * (nb_input + 1);
    int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1) / 2);

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1) / 2] beta_axi = beta_i[(idx_yx + 1):idx_ax];

    // Call the corresponding functions
    beta_yxi_v  = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v  = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

    for (t in 1:nb_T) {
      /***********************************yield and input *********************/
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx;
      int start_l = 1;
      for(l in 1:(nb_input + 1)){
        regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
        start_l = start_l + nb_crop;
      }
      // Call mat_select_reg_obs function
      int N_p_obs = sum(regimeyx);
      int N_p_mis = nb_crop * (nb_input + 1) - N_p_obs;
      matrix[N_p_obs, nb_crop * (nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop , (nb_input + 1)] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);
      yx_hat[t] = to_vector(yxapit)';
      yx_zero_hat[t] = (select_yx_obs' * select_yx_obs * to_vector(yxapit))';


      /*******************************acreage model******************************/
      // Computation of crops' margin
      matrix[nb_crop, nb_input + 1] piwit = to_matrix(piw[t], nb_crop, nb_input + 1);
      matrix[nb_crop, nb_input + 1] marge_yx_tp = yxapit .* piwit;
      vector[nb_crop ] marge_yx = sub[t]' + marge_yx_tp[,1] - marge_yx_tp[, 2:(nb_input + 1)] * rep_vector(1.0, nb_input) ;
      marge_yx_hat[t] = marge_yx';
    }
  }

}

