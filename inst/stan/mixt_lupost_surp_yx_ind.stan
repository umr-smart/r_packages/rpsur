#include include/myfunctions.stan

// The input data is a vector 'y' of length 'N'.
data {
  int<lower=0> nb_crop;
  int<lower=0> nb_input;
  int<lower=0> nb_T;
  int<lower=0> nb_rp;
  matrix[nb_T, nb_crop] s;
  matrix[nb_T, nb_input * nb_crop] x;
  matrix[nb_T, nb_crop] y;
  matrix[nb_T, nb_crop * nb_input] iw_p;
  matrix[nb_T, nb_crop * (nb_input + 1)] piw;
  int<lower=0> K;  // number of mixture components
  vector[K] lambda;
  matrix[nb_rp, K] mu;
  matrix[nb_rp * K, nb_rp] SIGMA;
  vector[nb_T] wdata;
  matrix[nb_crop * (nb_input + 1) , nb_crop * (nb_input + 1)] omega_u_yx;
  matrix[nb_T, nb_crop * (nb_input + 1)] zdelta_yx;
  real lo_spher_x;
  real up_spher_x;
  int distrib_method_beta_yx;
  int distrib_method_alpha_x;
  int sur_method;
}

transformed data{
  array[K] matrix[nb_rp, nb_rp] sigma;
  int start_t = 1;
  for(l in 1:nb_T){
    sigma[l] = SIGMA[start_t:(start_t + nb_rp - 1), ];
    start_t = start_t + nb_crop * (nb_input + 1);
  }
}

// The parameters accepted by the model. Our model
// accepts two parameters 'mu' and 'sigma'.
parameters {
  vector[nb_rp] beta;
}

model {
  vector[K] log_lambda = log(lambda);  // cache log calculation
  vector[K] lps = log_lambda;
  for (k in 1:K) {
    lps[k] += multi_normal_lpdf(beta | mu[, k], sigma[k]);
  }
  target += log_sum_exp(lps);

  target +=  dens_surp_yx(s,
                          x,
                          y,
                          iw_p,
                          piw,
                          wdata,
                          zdelta_yx,
                          omega_u_yx,
                          nb_crop,
                          nb_input,
                          lo_spher_x,
                          up_spher_x,
                          distrib_method_beta_yx,
                          distrib_method_alpha_x,
                          sur_method,
                          beta);

}

