functions {

  // Function to convert elements of a vector to regime values (1 or 0)
  //  regime_name takes a vector x as input.
  // N is the size of the input vector.
  // result is a vector of the same size as x that will hold the regime values.
  // The loop iterates over each element of x, checking if it's greater than 0.
  // If an element of x is greater than 0, the corresponding element in result is set to 1; otherwise, it's set to 0.
  // Finally, the function returns the result vector containing regime values.

  array[] int regime_name(row_vector x) {
    int N = size(x);
    array[N] int result;
    for (i in 1:N) {
      if (x[i] > 0) {
        result[i] = 1;
      } else {
        result[i] = 0;
      }
    }
    return result;
  }

  // Function to compute the transformation of beta_x
  matrix mod_rp_fun_beta_x(vector beta_bx, int nb_crop, int nb_input, int distrib_method_beta_x) {

    matrix[nb_crop, nb_input] m_beta;
    if (distrib_method_beta_x == 1) {
      vector[nb_crop * nb_input] beta = exp(beta_bx);
      m_beta = to_matrix(beta, nb_crop, nb_input);

    } else if (distrib_method_beta_x == 2) {
      vector[nb_crop * (nb_input + 1)] beta = beta_bx;
      m_beta = to_matrix(beta, nb_crop, nb_input);

    } else if (distrib_method_beta_x == 3) {
      vector[nb_crop * nb_input] beta; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
      for(i in 1:nb_crop * nb_input){
        beta[i] = fmax(0, beta_bx[i] );
      }
      m_beta = to_matrix(beta, nb_crop, nb_input);
    }

    return m_beta;
  }


  // Function to compute the transformation of beta_y and beta_x
  matrix mod_rp_fun_beta_xy(vector beta_byx, int nb_crop, int nb_input, int distrib_method_beta_yx) {

    matrix[nb_crop, (nb_input + 1)] m_beta;
    if (distrib_method_beta_yx == 1) {
      vector[nb_crop * (nb_input + 1)] beta = exp(beta_byx);
      m_beta = to_matrix(beta, nb_crop, (nb_input + 1));

    } else if (distrib_method_beta_yx == 2) {
      vector[nb_crop * (nb_input + 1)] beta = beta_byx;
      m_beta = to_matrix(beta, nb_crop, (nb_input + 1));

    } else if (distrib_method_beta_yx == 3) {
      vector[nb_crop * (nb_input + 1)] beta; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
      for(i in 1:nb_crop * (nb_input + 1)){
        beta[i] = fmax(0, beta_byx[i] );
      }
      m_beta = to_matrix(beta, nb_crop, (nb_input + 1));
    }

    return m_beta;
  }


  int symmat_size(int n) {
    int sz;
    // This calculates it iteratively because Stan gives a warning
    // with integer division.
    sz = 0;
    for (i in 1:n) {
      sz = sz + i;
    }
    return sz;
  }

  matrix vector_to_symmat(vector x, int n) {
    matrix[n, n] m;
    int k;
    k = 1;
    for (j in 1:n) {
      for (i in 1:j) {
        m[i, j] = x[k];
        if (i != j) {
          m[j, i] = m[i, j];
        }
        k = k + 1;
      }
    }
    return m;
  }
  //
  matrix dupmatrixrowvec(matrix X, array[] int m){
    matrix[sum(m),cols(X)] Xout;
    int next_row = 1;
    for(i in 1:rows(X)){
      if(m[i] < 0) {
        reject("m has to be positive");
      }
      Xout[next_row:(next_row + m[i] - 1)] = rep_matrix(X[i,], m[i]);
      next_row = next_row + m[i];
    }
    return Xout;
  }

  //
  vector symmat_to_vector(matrix x) {
    vector[symmat_size(rows(x))] v;
    int k;
    k = 1;
    // if x is m x n symmetric, then this will return
    // only parts of an m x m matrix.
    for (j in 1:rows(x)) {
      for (i in 1:j) {
        v[k] = x[i, j];
        k = k + 1;
      }
    }
    return v;
  }

  // Function to compute m_g_i for a given g_i matrix using specified parameters
  matrix m_g_ax_fun(matrix g_i_M, int nb_crop, int nb_input, real lo_spher, real up_spher) {

    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] m_g_i;

    // Iterate over each crop
    for (c in 1:nb_crop) {
      // Extract relevant components from g_i_M
      vector[nb_input] d_i = exp(g_i_M[c, 1:nb_input]');
      vector[(nb_input - 1) * nb_input  / 2] val_ci = g_i_M[c, (nb_input + 1):cols(g_i_M)]';

      // Compute c_i
      vector[(nb_input - 1) * nb_input  / 2] c_i = rep_vector(lo_spher, (nb_input - 1) * nb_input  / 2) + (up_spher - lo_spher) * inv_logit(val_ci);

      // Split c_i into groups
     // vector[nb_input - 1] list_c[nb_input - 1];
      array[nb_input - 1] vector[nb_input - 1] list_c;
      for (i in 1:(nb_input - 1)) {
        list_c[i] = c_i[((i - 1) * i  / 2 + 1):(i * (i + 1)  / 2)];
      }

      // Initialize val matrix
      matrix[nb_input, nb_input] val = rep_matrix(0, nb_input, nb_input);
      val[1, 1] = d_i[1];

      // Compute val matrix elements
      for (i in 2:nb_input) {
        val[i, 1] = d_i[i] * cos(list_c[i - 1][1]);
        for (j in 2:i) {
          if (i == j) {
            val[i, j] = d_i[i] * prod(sin(list_c[i - 1][1:(j - 1)]));
          } else {
            val[i, j] = d_i[i] * prod(sin(list_c[i - 1][1:(j - 1)])) * cos(list_c[i - 1][j]);
          }
        }
      }

      // Compute m_g_i_tp
      matrix[nb_input, nb_input] m_g_i_tp = tcrossprod(val); //val * val';
      m_g_i_tp = (m_g_i_tp + m_g_i_tp') / 2.0;  // Symmetrize

      // Store m_g_i_tp as a vector
      m_g_i[c] = symmat_to_vector(m_g_i_tp)';
    }

    return m_g_i;
  }

  // Function to compute the transformation of alpha_x for each crop
  matrix mod_rp_fun_alpha_x(vector beta_ax, int nb_crop, int nb_input, real lo_spher, real up_spher, int distrib_method_alpha) {

    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] m_g_i;
    matrix[nb_input, nb_input] m_g_i_tp;
    matrix[nb_input, nb_input] result;

    // Transform g_i vector into a matrix
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] g_i_M = to_matrix(beta_ax, nb_crop, nb_input * (nb_input + 1)  / 2, 0);

    if (distrib_method_alpha == 1) {
      // Compute m_g_i using m.g.ax.fun
        m_g_i = m_g_ax_fun(g_i_M, nb_crop, nb_input, lo_spher, up_spher);
    } else if (distrib_method_alpha == 2) {
      // Compute m_g_i using inverse vech transformation
      for (c in 1:nb_crop) {
        m_g_i_tp = vector_to_symmat( to_vector(g_i_M[c]), nb_input);

        for (i in 1:nb_input) {
          for (j in 1:nb_input) {
            if (i == j) {
               result[i, j] = exp(m_g_i_tp[i, j]);
            } else if (i < j){
               result[i, j] = 0;
            } else{
               result[i, j] = m_g_i_tp[i, j];
            }
          }
        }

        m_g_i_tp =  tcrossprod(result); //result * result';
        m_g_i_tp =  (m_g_i_tp + m_g_i_tp') / 2.0;
        m_g_i[c] = symmat_to_vector(m_g_i_tp)';
      }
    }

    return m_g_i;
  }


  // Selection Matrix of yield or input variable equations: observed
  matrix mat_select_reg_obs(array[] int regime) {
    int N = size(regime);
    int N_p = sum(regime);
    matrix[N, N] mat_tp = diag_matrix(rep_vector(1, N));
    matrix[N_p, N] val;

    val = rep_matrix(0, N_p, N); // Initialize val with zeros

    int idx = 1;
    for (i in 1:N) {
      if (regime[i] > 0) {
        val[idx] = mat_tp[i];
        idx += 1;
      }
    }
    return val;
  }

  // Selection Matrix of yield or input variable equations: missing
  matrix mat_select_reg_mis(array[] int regime) {
    int N = size(regime);
    int N_p = N - sum(regime);
    matrix[N, N] mat_tp = diag_matrix(rep_vector(1, N));
    matrix[N_p, N] val;

    val = rep_matrix(0, N_p, N); // Initialize val with zeros

    int idx = 1;
    for (i in 1:N) {
      if (regime[i] == 0) {
        val[idx] = mat_tp[i];
        idx += 1;
      }
    }
    return val;
  }

  //
  matrix approx_yxit_fun(matrix w_pit, matrix beta_yxit, matrix m_g_it, int nb_crop, int nb_input) {
    matrix[nb_crop, nb_input + 1] yxapit;

    for (c in 1:nb_crop) {
      matrix[nb_input , nb_input] g_i = vector_to_symmat(m_g_it[c]', nb_input) ;  //
      yxapit[c, 1] = beta_yxit[c, 1] - 0.5 * dot_product(w_pit[c], g_i * w_pit[c]')  ; //w_pit[c] * g_i * w_pit[c]'

      for (j in 1:nb_input) {
        yxapit[c, j + 1] = beta_yxit[c, j + 1] - dot_product(w_pit[c], g_i[j]') ;
      }
    }
    return yxapit;
  }

   //
  real dens_surp_yx(
              matrix s,
              matrix x,
              matrix y,
              matrix iw_p,
              matrix piw,
              vector wdata,
              matrix zdelta_yx,
              matrix omega_u_yx,
              int nb_crop,
              int nb_input,
              real lo_spher_x,
              real up_spher_x,
              int distrib_method_beta_yx,
              int distrib_method_alpha_x,
              int sur_method,
              vector beta_i) {

    int nb_T = rows(y);
    real log_likelihood = 0;

    if(sur_method == 1){
      int idx_yx = nb_crop * (nb_input + 1);
      vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];

      matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
      beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);

      for (t in 1:nb_T) {
        // Extract regime name and create regimeyx
        array[nb_crop] int regimey = regime_name(s[t]);
        array[nb_crop + nb_crop * nb_input] int regimeyx;
        int start_l = 1;
        for(l in 1:(nb_input + 1)){
          regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
          start_l = start_l + nb_crop;
        }

        // Call mat_select_reg_obs function
        int N_p = sum(regimeyx);
        matrix[N_p, nb_crop * (nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

        // Compute omega_u_yx_obs
        matrix[nb_crop * (nb_input + 1), nb_crop * (nb_input + 1)] omega_u_yx_t = omega_u_yx / wdata[t];
        matrix[N_p, N_p] omega_u_yx_obs = quad_form(omega_u_yx_t, select_yx_obs') ;//select_yx_obs * omega_u_yx_t * select_yx_obs';

        // Extract zdelta_yxit for current time step
        matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

        // Compute yxapit
        matrix[nb_crop, nb_input + 1] yxapit = beta_yxi_v + zdelta_yxit;

        // Compute error terms u_y and u_x
        matrix[nb_crop, nb_input + 1] u_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

        // Compute u_yx_obs
        vector[N_p] u_yx_obs = select_yx_obs * to_vector(u_yx);

        // Compute log likelihood using dmnorm function
        real log_likelihood_xy_obs = multi_normal_lpdf(u_yx_obs | rep_vector(0, N_p), omega_u_yx_obs);

        // log likelihood
        log_likelihood += log_likelihood_xy_obs ;
      }
    }else if(sur_method == 2){
      int idx_yx = nb_crop * (nb_input + 1);
      int idx_px = idx_yx + nb_crop * nb_input;

      vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
      vector[nb_crop * nb_input] beta_pxi = beta_i[(idx_yx + 1):idx_px];

      matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
      matrix[nb_crop, nb_input] beta_pxi_v;
      beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
      beta_pxi_v = to_matrix(beta_pxi, nb_input, nb_crop)';

      for (t in 1:nb_T) {
        // Extract regime name and create regimeyx
        array[nb_crop] int regimey = regime_name(s[t]);
        array[nb_crop + nb_crop * nb_input] int regimeyx;
        int start_l = 1;
        for(l in 1:(nb_input + 1)){
          regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
          start_l = start_l + nb_crop;
        }

        // Call mat_select_reg_obs function
        int N_p = sum(regimeyx);
        matrix[N_p, nb_crop *(nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

        // Compute omega_u_yx_obs
        matrix[nb_crop * (nb_input + 1), nb_crop *(nb_input + 1)] omega_u_yx_t = omega_u_yx / wdata[t];
        matrix[N_p, N_p] omega_u_yx_obs = quad_form(omega_u_yx_t, select_yx_obs') ;//select_yx_obs * omega_u_yx_t * select_yx_obs';

        // Extract zdelta_yxit for current time step
        matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

        //vector[nb_crop] xbeta_px = (to_matrix(x[t], nb_crop, nb_input) .* beta_pxi_v) * rep_vector(1.0, nb_input);
        vector[nb_crop] xbeta_px = rows_dot_product(to_matrix(x[t], nb_crop, nb_input), beta_pxi_v);

        matrix[nb_crop, nb_input + 1] xbeta_px_0 = append_col(xbeta_px, rep_matrix(0, nb_crop, nb_input));

        // Compute beta_yxit
        matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit + xbeta_px_0;
        //beta_yxit[,1] = beta_yxit[,1] + xbeta_px;

        // Compute yxapit using approx.yxit.fun
        matrix[nb_crop, nb_input + 1] yxapit = beta_yxit;

        // Compute error terms u_y and u_x
        matrix[nb_crop, nb_input + 1] u_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

        // Compute u_yx_obs
        vector[N_p] u_yx_obs = select_yx_obs * to_vector(u_yx);

        // Compute log likelihood using dmnorm function
        real log_likelihood_xy_obs = multi_normal_lpdf(u_yx_obs | rep_vector(0, N_p), omega_u_yx_obs);

        // log likelihood
        log_likelihood += log_likelihood_xy_obs ;
      }

    }else if (sur_method == 3){

      int idx_yx = nb_crop * (nb_input + 1);
      int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1)  / 2);
      vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
      vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = beta_i[(idx_yx + 1):idx_ax];

      matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
      matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;

      // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
      beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
      beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

      for (t in 1:nb_T) {
        // Extract regime name and create regimeyx
        array[nb_crop] int regimey = regime_name(s[t]);
        array[nb_crop + nb_crop * nb_input] int regimeyx;
        int start_l = 1;
        for(l in 1:(nb_input + 1)){
          regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
          start_l = start_l + nb_crop;
        }

        // Call mat_select_reg_obs function
        int N_p = sum(regimeyx);
        matrix[N_p, nb_crop *(nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

        // Compute omega_u_yx_obs
        matrix[nb_crop *(nb_input + 1), nb_crop *(nb_input + 1)] omega_u_yx_t = omega_u_yx / wdata[t];
        matrix[N_p, N_p] omega_u_yx_obs = quad_form(omega_u_yx_t, select_yx_obs') ;//select_yx_obs * omega_u_yx_t * select_yx_obs';

        // Extract zdelta_yxit for current time step
        matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

        // Compute beta_yxit
        matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

        matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

        // Compute yxapit using approx.yxit.fun
        matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);


        // Compute error terms u_y and u_x
        matrix[nb_crop, nb_input + 1] u_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

        // Compute u_yx_obs
        vector[N_p] u_yx_obs = select_yx_obs * to_vector(u_yx);

        // Compute log likelihood using dmnorm function
        real log_likelihood_xy_obs = multi_normal_lpdf(u_yx_obs | rep_vector(0, N_p), omega_u_yx_obs);

        // log likelihood
        log_likelihood += log_likelihood_xy_obs ;
      }
    }
    return log_likelihood;
  }
}
